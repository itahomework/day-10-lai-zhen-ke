package com.afs.restapi.service;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.repository.EmployeeJPARepository;
import com.afs.restapi.service.dto.EmployeeRequest;
import com.afs.restapi.service.dto.EmployeeResponse;
import com.afs.restapi.service.mapper.EmployeeMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {


    private final EmployeeJPARepository employeeJPARepository;

    public EmployeeService(EmployeeJPARepository employeeJPARepository) {
        this.employeeJPARepository = employeeJPARepository;
    }

    public List<EmployeeResponse> findAll() {
        return employeeJPARepository.findAllEmployeeResponse();
    }

    public EmployeeResponse findById(Long id) {
        Optional<Employee> employeeOptional = employeeJPARepository.findById(id);
        if (employeeOptional.isEmpty()){
            throw new EmployeeNotFoundException();
        }
        Employee employee = employeeOptional.get();
        return EmployeeMapper.toResponse(employee);
    }

    public EmployeeResponse update(Long id, EmployeeRequest employeeRequest) {
        Optional<Employee> employeeOptional = employeeJPARepository.findById(id);
        if (employeeOptional.isEmpty()){
            throw new EmployeeNotFoundException();
        }
        Employee toBeUpdatedEmployee = employeeOptional.get();
        if (employeeRequest.getSalary() != null) {
            toBeUpdatedEmployee.setSalary(employeeRequest.getSalary());
        }
        if (employeeRequest.getAge() != null) {
            toBeUpdatedEmployee.setAge(employeeRequest.getAge());
        }
        Employee savedEmployee = employeeJPARepository.save(toBeUpdatedEmployee);
        return EmployeeMapper.toResponse(savedEmployee);
    }

    public List<EmployeeResponse> findAllByGender(String gender) {
        return employeeJPARepository.findEmployeeResponseByGender(gender);
    }

    public EmployeeResponse create(EmployeeRequest employeeRequest) {
        Employee savedEmployee = employeeJPARepository.save(EmployeeMapper.toEntity(employeeRequest));
        return EmployeeMapper.toResponse(savedEmployee);
    }

    public List<EmployeeResponse> findByPage(Integer page, Integer size) {
        PageRequest pageRequest = PageRequest.of(page - 1, size);
        return employeeJPARepository.findAllEmployeeResponse(pageRequest).getContent();
    }

    public void delete(Long id) {
        employeeJPARepository.deleteById(id);
    }
}
