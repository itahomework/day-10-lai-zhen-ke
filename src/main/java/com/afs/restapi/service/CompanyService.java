package com.afs.restapi.service;

import com.afs.restapi.entity.Company;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.repository.CompanyJPARepository;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.service.dto.CompanyRequest;
import com.afs.restapi.service.dto.CompanyResponse;
import com.afs.restapi.service.mapper.CompanyMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CompanyService {

    private final CompanyJPARepository companyJPARepository;

    public CompanyService(CompanyJPARepository companyJPARepository) {
        this.companyJPARepository = companyJPARepository;
    }


    public List<CompanyResponse> findAll() {
        return companyJPARepository.findAllCompanyResponse();
    }

    public List<CompanyResponse> findByPage(Integer page, Integer size) {
        PageRequest pageRequest = PageRequest.of(page - 1, size);
        return companyJPARepository.findAllCompanyResponse(pageRequest).getContent();
    }

    public CompanyResponse findById(Long id) {
        return companyJPARepository.findCompanyResponseById(id);
    }

    public CompanyResponse update(Long id, CompanyRequest companyRequest) {
        Optional<Company> companyOptional = companyJPARepository.findById(id);
        if (companyOptional.isEmpty()){
            throw new CompanyNotFoundException();
        }
        Company oldCompany = companyOptional.get();
        oldCompany.setName(companyRequest.getName());
        Company savedCompany = companyJPARepository.save(oldCompany);
        return CompanyMapper.toResponse(savedCompany);
    }

    public CompanyResponse create(CompanyRequest companyRequest) {
        Company savedCompany = companyJPARepository.save(CompanyMapper.toEntity(companyRequest));
        return CompanyMapper.toResponse(savedCompany);
    }

    public void delete(Long id) {
        companyJPARepository.deleteById(id);
    }
}
