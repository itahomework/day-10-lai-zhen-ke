package com.afs.restapi.repository;

import com.afs.restapi.entity.Company;
import com.afs.restapi.service.dto.CompanyResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompanyJPARepository extends JpaRepository<Company, Long> {

    @Query(value = "select new com.afs.restapi.service.dto.CompanyResponse(c.id, c.name, count(e.companyId)) from Company c" +
            " left join Employee e on e.companyId = c.id group by c.id")
    List<CompanyResponse> findAllCompanyResponse();

    @Query(value = "select new com.afs.restapi.service.dto.CompanyResponse(c.id, c.name, count(e.companyId)) from Company c" +
            " left join Employee e on e.companyId = c.id where c.id = ?1 group by c.id")
    CompanyResponse findCompanyResponseById(Long id);


    @Query(value = "select new com.afs.restapi.service.dto.CompanyResponse(c.id, c.name, count(e.companyId)) from Company c" +
            " left join Employee e on e.companyId = c.id group by c.id")
    Page<CompanyResponse> findAllCompanyResponse(Pageable pageable);
}
