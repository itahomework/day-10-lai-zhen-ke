INSERT INTO companies (name) VALUES ('Baidu');
INSERT INTO employees (name, age, gender, salary, company_id) VALUES ('John Smith', 32, 'Male', 5000.0, 1);
INSERT INTO employees (name, age, gender, salary, company_id) VALUES ('Jane Johnson', 28, 'Female', 6000.0, 1);
INSERT INTO employees (name, age, gender, salary, company_id) VALUES ('David Williams', 35, 'Male', 5500.0, 1);
INSERT INTO employees (name, age, gender, salary, company_id) VALUES ('Emily Brown', 23, 'Female', 4500.0, 1);
INSERT INTO employees (name, age, gender, salary, company_id) VALUES ('Michael Jones', 40, 'Male', 7500.0, 1);
INSERT INTO employees (name, age, gender, salary, company_id) VALUES ('Sarah Davis', 29, 'Female', 6200.0, 1);
