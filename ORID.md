### daily report(07/21)

- O: learn flyway for database migration, mapper and cloud native.
- R: benefit a lot.
- I: I used to migrate databases by manually exporting .sql files and then manually importing them into a new database. If there are many databases, it's very troublesome. Flyway also provides version control, and all operations we make on the database are saved. Previously, when I used MyBatis, I usually handwritten SQL to select the attributes that I needed to query the database. MyBatis would automatically map to VO, but when I used Spring Data JPA, everything found in the repository was mapped to Entity instead of VO. Finally,  concepts about cloud native are abstract to me.
- D: need more understanding of cloud native concepts